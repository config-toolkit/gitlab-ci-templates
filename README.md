# Gitlab CI Templates

## Table of contents

1. [Introduction](#introduction)
2. [How to use](#how-to-use)
3. [Available configurations](#available-configurations)
    - [Node.js Merge Request](#nodejs-merge-request)
        - [Description](#description)
        - [Available options](#available-options)

## Introduction

_The purpose of the repository is to centralized my Gitlab CI configurations. The main idea is to constructs generic and dynamically configurable files that would be reusable to avoid to always rewrite the same files for each projects. All this configurations are available to use for everyone. Below, you will find a table of contents that describe all the existing configuration existing in the repository, their purpose and the descriptions of the available options that can be customizable to have the exact behavior to fit with your specific needs._

## How to use
The configurations presents on this repository can be include on your projects by making reference of them in your own gitlab-ci.yml of your project. 

``` yaml
include:
  - project: "config-toolkit/gitlab-ci-templates" # The url of this repository
    ref: main # The reference to the main branch of this repository
    file: "node.merge-requests.gitlab-ci.yml" # The name of the configuration file you want to implement
```

All the configurations in this repository have a default implementation. This default behavior can be modified with overriding the default value of environment variables. Follow the documentation of the specific configuration of your choice below to see all the possible customization available to implement the behaviour that your project need.

``` yaml
variables:
  VARIABLE_NAME_1: variableValue1
  VARIABLE_NAME_2: "variableValue2"
```

## Available Configurations

### Node.js Merge Request

#### Description
The purpose of this configuration file is to implement Gitlab CI/CD configuration inside Node.js projects. The jobs will be activated when a merge request is created or updated. It allow the developer to have a choice in the implementation of multiple jobs that are inherent to a Node.js project to keep it coherent, secured and uniformized.

The configuration is implemented with a cache functionnality to improve the performance of the jobs execution.

This configuration contains :<br>
    - **INSTALL :** *will install the dependencies of a project via the `npm ci` command.*<br>
    - **CHECK-VULNERABILITIES :** *will check if the project have some vulnerabilities relative to one of the packages via the `npm audit` command. Even if so, the job don't result of an error and just provide informations about these potential vulnerabilities.*<br>
    - **LINT :** *will run a lint on the files of the project via the `npm run` command. For this job to works, you will need to implement a lint script on the package.json of your project.*<br>
    - **RUN-UNIT-TESTS :** *will run the unit tests implemented in your project via the `npm run` command. For this job to works, you will need to implement a unit tests script ont the package.json of your project.*<br>
    - **RUN-E2E-TESTS :** *will run the tests end-to-end implemented in your project via the `npm run` command. For this job to works, you will need to implement a end-to-end tests script ont the package.json of your project.*<br>    

#### Available options
To change the behavior of the jobs, you can change the value of the environment variables available for this configuration. The value displayed below are the default value.<br>
<br>
  `NODEJS_IMAGE` : `node:latest` *(existing Node.js or Node.js-alpine image)*<br>
  `E2E_TEST_COMMAND`: `test:e2e` *(reference to the script in the package.json of your project)*<br>
  `E2E_TEST_ENABLED`: `"true" | "false"`<br>
  `LINT_COMMAND`: `lint` *(reference to the script in the package.json of your project)*<br>
  `LINT_ENABLED`: `"true" | "false"`<br>
  `UNIT_TEST_COMMAND`: `test` *(reference to the script in the package.json of your project)*<br>
  `UNIT_TEST_ENABLED`: `"true" | "false"`<br>


#### Example
``` yaml
variables:
  NODEJS_IMAGE: node:20.10.0-alpine
  E2E_TEST_ENABLED: "false" # (will not run the job)
  UNIT_TEST_ENABLED: "true"

include:
  - project: "config-toolkit/gitlab-ci-templates"
    ref: main
    file: "node.merge-requests.gitlab-ci.yml"
```